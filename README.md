# Description

- The goal of the script NHANES_PARSER is to produce a cleaned NAHNES dataset for which the mortality information exist.
- The script downalods the mortality information files (as specified in the NHANES_MORT.csv) from the ftp server and store them in the **ftp_files**
- After that the entire dataset is merged and uneligible entrie are removed. This resuts into a clean mortality dataset.
- For these patients, the other biomarker files are screened and finally the cleaned dataset (for each biomarkers) sis stored in **csv_files/cleaned_dataset**

